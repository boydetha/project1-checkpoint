<?php

namespace Steampunked;

class SteampunkedView
{


    public function __construct(SteampunkedModel $model) {
        $this->model =$model;
        $this->size = $this->model->getSize();
        $this->p1 = $this->model->getPlayer1Name();
        $this->p2 = $this->model->getPlayer2Name();
    }

    public function present_grid(){

        //placing the start and end points
//        $this->size=6;
        $pt=0;
        if($this->size==10){
            $pt=1;
        }else if($this->size==20){
            $pt=3;
        }

        $startPoints = array
        (
            array($pt,0),
            array($this->size-($pt)-1,0)
        );
        $endPointsValve = array
        (
            array($pt+1,$this->size-1),
            array($this->size-($pt)-2,$this->size-1)
        );
        $endPointsTop = array
        (
            array($pt,$this->size-1),
            array($this->size-($pt)-3,$this->size-1)
        );





        $html= '<form method="post" action="game-post.php">' . '<div class="game">';

        for($i=0;$i<$this->size;++$i) {
            $html .='<div class="row">';

            for($j=0;$j<$this->size;++$j){
                $html.= '<div class="cell">';

                if(in_array(array($i,$j), $startPoints)){
                    $html.= '<img src="./images/valve-closed.png"  width="50" height="50" alt="closed valve">';
                } else if(in_array(array($i,$j), $endPointsValve)){
                    $html.= '<img src="./images/gauge-0.png"  width="50" height="50" alt="gauge at 0">';
                } else if(in_array(array($i,$j), $endPointsTop)){
                    $html.= '<img src="./images/gauge-top-0.png"  width="50" height="50" alt="gauge at 0 top">';
                }
                else if($i==$pt  &&$j<$this->size-2){
                    //testing pipes on screen
                    $html.= '<img src="./images/straight-h.png"  width="50" height="50" alt="straight pipe">';
                }
                else if($i==($pt) &&$j>$this->size-3){
                    $html.= '<img src="./images/ninety-sw.png"  width="50" height="50" alt="ninety angle pipe sw">';
                }else if($i==($pt+1) &&$j>$this->size-3){
                    $html.= '<img src="./images/ninety-ne.png"  width="50" height="50" alt="nienty angle pipe ne">';
                }


                $html.='</div>';

            }

            $html .='</div>';
        }

        $html.='</div>';
        $html.=$this->present_buttons();
        $html.='</form>';

        return $html;

    }
    public function present_turn() {
        $n = ($this->model->getTurnNum() == 0) ? $this->model->getPlayer1Name() : $this->model->getPlayer2Name();
        return "<h1>It is $n's turn</h1>";
    }

    public function present_winner() {
        $n = ($this->model->getTurnNum() == 0) ? $this->model->getPlayer2Name() : $this->model->getPlayer1Name();
        return "<h1>$n wins!</h1>";
    }

    public function present_pieces() {
    }

    private function present_buttons() {
        $html='<p><input type="submit" name="Rotate" value="Rotate"><input type="submit" name="Discard" value="Discard">
                  <input type="submit" name="OpenValve" value="Open Valve"><input type="submit" name="GiveUp" value="Give Up"></p>';
        return $html;
    }

    public function present_inputs(){
        return "<p> Player 1 is $this->p1, Player 2 is $this->p2, and size is $this->size</p>"; //Testing controller
    }

    private $p1;
    private $p2;
    private $size =6;
    private $model;
}