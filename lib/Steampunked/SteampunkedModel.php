<?php

namespace Steampunked;

class SteampunkedModel {

    private $size = 0;
    private $turn = 0;
    private $p1name = "Default One"; //testing controller
    private $p2name = "Default Two"; //testing controller

    const PLAYERONE = 0;
    const PLAYERTWO = 1;

    /**
     * SteampunkedModel constructor.
     */
    public function __construct($seed = null) {
        if($seed === null) {
            $seed = time();
        }
    }

    public function getSize() {
        return $this->size;
    }

    public function setSize($s) {
        if($s == 6 || $s == 10 || $s == 20) {
            $this->size = $s;
        } else {
            $this->size = 0;
        }
    }

    public function switchTurn() {
        $this->turn += 1;
    }

    public function getPlayerTurn() {
        if($this->turn % 2 == 0) {
            return self::PLAYERONE;
        } else {
            return self::PLAYERTWO;
        }
    }

    public function getTurnNum() {
        return $this->turn;
    }

    public function setPlayer1Name($n1) {
        $this->p1name = $n1;
    }

    public function setPlayer2Name($n2) {
        $this->p2name = $n2;
    }

    public function getPlayer1Name() {
        return $this->p1name;
    }

    public function getPlayer2Name() {
        return $this->p2name;
    }

    public function rotate() {

    }
}