<?php

namespace Steampunked;

class SteampunkedController
{
    public function __construct(SteampunkedModel $steampunked, $post) {
        $this->steampunked = $steampunked;

        if(isset($post['NewGame'])) {
            $this->reset = true;
            $this->page = 'index.php';
        }
        else if(isset($post['StartGame'])) {
            $this->steampunked->setPlayer1Name(strip_tags($post["p1"]));
            $this->steampunked->setPlayer2Name(strip_tags($post["p2"]));
            $this->steampunked->setSize(strip_tags($post["gamesize"]));
        }
        else if(isset($post["Rotate"])) {
            //$this->steampunked->rotate();
        }
        else if(isset($post["GiveUp"])) {
            $this->page = 'winlose.php';
        }
    }

    public function getPage() {
        return $this->page;
    }

    public function isReset() {
        return $this->reset;
    }

    private $page = 'game.php';     // The next page we will go to
    private $steampunked;           // The SteampunkedModel object we are controlling
    private $reset = false;         // True if we need to reset the game
}