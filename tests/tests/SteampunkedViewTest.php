<?php

require __DIR__ . "/../../vendor/autoload.php";

/** @file
 * @brief Empty unit testing Steampunked view
 * @cond 
 * @brief Unit tests for the class 
 */
class SteampunkedViewTest extends \PHPUnit_Framework_TestCase
{
	public function test_constructor(){
        $steampunked = new Steampunked\SteampunkedModel();
        $view = new \Steampunked\SteampunkedView($steampunked);

        $this->assertInstanceOf('Steampunked\SteampunkedView', $view);
	}

    public function test_presentgrid(){
        $steampunked = new Steampunked\SteampunkedModel();



        $steampunked->setSize(6);
        $view = new \Steampunked\SteampunkedView($steampunked);
        $this->assertContains("game",$view->present_grid());

        $cellCount=substr_count($view->present_grid(),'cell');

        $this->assertEquals(36,$cellCount);

        $steampunked->setSize(10);
        $view = new \Steampunked\SteampunkedView($steampunked);
        $this->assertContains("game",$view->present_grid());

        $cellCount=substr_count($view->present_grid(),'cell');

        $this->assertEquals(100,$cellCount);

        $steampunked->setSize(20);
        $view = new \Steampunked\SteampunkedView($steampunked);
        $this->assertContains("game",$view->present_grid());

        $cellCount=substr_count($view->present_grid(),'cell');

        $this->assertEquals(400,$cellCount);

        $buttons='<input type="submit" name="Rotate" value="Rotate"><input type="submit" name="Discard" value="Discard">
                  <input type="submit" name="OpenValve" value="Open Valve"><input type="submit" name="GiveUp" value="Give Up">';

        $this->assertContains($buttons,$view->present_grid());
    }
}

/// @endcond
?>
