<?php

require __DIR__ . "/../../vendor/autoload.php";

/** @file
 * @brief Empty unit testing Steampunked view
 * @cond 
 * @brief Unit tests for the class 
 */
class SteampunkedControllerTest extends \PHPUnit_Framework_TestCase
{
	public function test_constructor() {
		$steampunked = new Steampunked\SteampunkedModel();
        $controller = new Steampunked\SteampunkedController($steampunked, array('StartGame' => 'Start Game', 'p1' => 'John',
            'p2' => 'Jane', 'gamesize' => 6));
		$this->assertInstanceOf('Steampunked\SteampunkedController', $controller);
        $this->assertEquals('John', $steampunked->getPlayer1Name());
        $this->assertEquals('Jane', $steampunked->getPlayer2Name());
        $this->assertEquals(6, $steampunked->getSize());
        $this->assertEquals('game.php', $controller->getPage());
	}
	public function test_reset() {
		$steampunked = new Steampunked\SteampunkedModel();
		$controller = new Steampunked\SteampunkedController($steampunked, array('StartGame' => 'Start Game', 'p1' => 'John',
			'p2' => 'Jane', 'gamesize' => 6));
		$this->assertFalse($controller->isReset());

		$controller = new Steampunked\SteampunkedController($steampunked, array('NewGame' => 'New Game'));
		$this->assertTrue($controller->isReset());

        $steampunked = new Steampunked\SteampunkedModel();
        $controller = new Steampunked\SteampunkedController($steampunked, array('Rotate' => 'Rotate'));
        $this->assertFalse($controller->isReset());

        $controller = new Steampunked\SteampunkedController($steampunked, array('GiveUp' => 'Give Up'));
        $this->assertFalse($controller->isReset());
	}
}

/// @endcond
?>
