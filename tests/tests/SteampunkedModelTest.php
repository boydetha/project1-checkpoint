<?php

require __DIR__ . "/../../vendor/autoload.php";

/** @file
 * @brief Empty unit testing template
 * @cond 
 * @brief Unit tests for the class 
 */
class SteampunkedModelTest extends \PHPUnit_Framework_TestCase
{

	public function test_construct() {
		$steampunked = new Steampunked\SteampunkedModel();
		$this->assertInstanceOf("Steampunked\SteampunkedModel", $steampunked);

		$this->assertEquals(0, $steampunked->getSize());
		$this->assertEquals($steampunked::PLAYERONE, $steampunked->getPlayerTurn());
	}

	public function test_setSize() {
		$steampunked = new Steampunked\SteampunkedModel();

		$steampunked->setSize(6);
		$this->assertEquals(6, $steampunked->getSize());
		$this->assertEquals($steampunked::PLAYERONE, $steampunked->getPlayerTurn());

		$steampunked->switchTurn();
		$steampunked->setSize(10);
		$this->assertEquals(10, $steampunked->getSize());
		$this->assertEquals($steampunked::PLAYERTWO, $steampunked->getPlayerTurn());

		$steampunked->switchTurn();
		$steampunked->setSize(20);
		$this->assertEquals(20, $steampunked->getSize());
		$this->assertEquals($steampunked::PLAYERONE, $steampunked->getPlayerTurn());

		$steampunked->switchTurn();
		$steampunked->setSize(14);
		$this->assertEquals(0, $steampunked->getSize());
		$this->assertEquals($steampunked::PLAYERTWO, $steampunked->getPlayerTurn());
	}

	public function test_turn() {
		$steampunked = new Steampunked\SteampunkedModel();
		$this->assertEquals($steampunked::PLAYERONE, $steampunked->getPlayerTurn());
		$this->assertEquals(0, $steampunked->getTurnNum());

		$steampunked->switchTurn();
		$this->assertEquals($steampunked::PLAYERTWO, $steampunked->getPlayerTurn());
		$this->assertEquals(1, $steampunked->getTurnNum());

		$steampunked->switchTurn();
		$this->assertEquals($steampunked::PLAYERONE, $steampunked->getPlayerTurn());
		$this->assertEquals(2, $steampunked->getTurnNum());

		$steampunked->switchTurn();
		$this->assertEquals($steampunked::PLAYERTWO, $steampunked->getPlayerTurn());
		$this->assertEquals(3, $steampunked->getTurnNum());
	}
}

/// @endcond
?>
