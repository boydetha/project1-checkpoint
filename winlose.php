<?php
require 'format.inc.php';
require 'lib/game.inc.php';
$view = new \Steampunked\SteampunkedView($steampunked);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Game Over!</title>
    <link href="Steampunked.css" type="text/css" rel="stylesheet" />
</head>
<body>
<?php echo present_header(); ?>

<?php echo $view->present_grid(); ?>

<?php echo $view->present_winner(); ?>

<form method="post" action="game-post.php">
<p id="newgamebutton"><input type="submit" name="NewGame" value="New Game"></p></form>

</body>
</html>