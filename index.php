<?php
require 'format.inc.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome to Steampunked!</title>
    <link href="Steampunked.css" type="text/css" rel="stylesheet" />
</head>
<body>
<?php echo present_header(); ?>

<form method="post" action="game-post.php"><fieldset>
<h1>Start a New Game</h1>
<p><label for="p1">Player 1:</label> <input type="text" name="p1" id="p1" placeholder=" Enter Player 1's Name"></p>
<p><label for="p2">Player 2:</label> <input type="text" name="p2" id="p2" placeholder=" Enter Player 2's Name"></p>
<label for="gamesize">Choose the game size:</label>
<select name = "gamesize">
    <option value="6by6">6 by 6</option>
    <option value="10by10">10 by 10</option>
    <option value="20by20">20 by 20</option>
</select>
<p><input type="submit" name="StartGame" value="Start Game"></p></fieldset></form>

</body>
</html>




