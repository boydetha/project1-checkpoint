<?php
/*
 * Create the HTML for the header block
 * @return string HTML for the header block
 */
function present_header() {
    $html = <<<HTML
    <header>
    <p id="title"><img src="./images/title.png" width="600" height="104" alt="Steampunked logo"></p>
    </header>
HTML;
    return $html;
}
?>