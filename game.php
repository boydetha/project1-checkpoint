<?php
/**
 * Created by PhpStorm
 * User: grovecha
 * Date: 2/13/2016
 * Time: 11:54 AM
 */
require 'format.inc.php';
require 'lib/game.inc.php';
$view = new \Steampunked\SteampunkedView($steampunked);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Game</title>
    <link href="Steampunked.css" type="text/css" rel="stylesheet" />
</head>
<body>
<!---->
<?php echo present_header(); ?>

<?php echo $view->present_grid();?>

<?php echo $view->present_turn();?>





</body>
</html>